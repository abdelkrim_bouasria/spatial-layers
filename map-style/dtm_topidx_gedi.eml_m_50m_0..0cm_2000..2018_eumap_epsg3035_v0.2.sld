<?xml version="1.0" ?>
<sld:StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint/>
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>dtm_topidx_gedi.eml_m_50m_0..0cm_2000..2018_eumap_epsg3035_v0.2</sld:Name>
            <sld:Description>GRASS GIS topographic index (wetness index)</sld:Description>
            <sld:Title/>
            <sld:FeatureTypeStyle>
                <sld:Name/>
                <sld:Rule>
                    <sld:RasterSymbolizer>
                        <sld:Geometry>
                            <ogc:PropertyName>grid</ogc:PropertyName>
                        </sld:Geometry>
                        <sld:Opacity>1</sld:Opacity>
                        <sld:ColorMap>
                            <sld:ColorMapEntry color="#73ffd9" label="64" opacity="1.0" quantity="64"/>
                            <sld:ColorMapEntry color="#73ffe0" label="65" opacity="1.0" quantity="64.6929"/>
                            <sld:ColorMapEntry color="#73ffe8" label="65" opacity="1.0" quantity="65.3899"/>
                            <sld:ColorMapEntry color="#73fff0" label="66" opacity="1.0" quantity="66.0828"/>
                            <sld:ColorMapEntry color="#73fff7" label="67" opacity="1.0" quantity="66.7798"/>
                            <sld:ColorMapEntry color="#73ffff" label="67" opacity="1.0" quantity="67.4727"/>
                            <sld:ColorMapEntry color="#77f7ff" label="68" opacity="1.0" quantity="68.1697"/>
                            <sld:ColorMapEntry color="#7bf0ff" label="69" opacity="1.0" quantity="68.8626"/>
                            <sld:ColorMapEntry color="#7fe8ff" label="70" opacity="1.0" quantity="69.5596"/>
                            <sld:ColorMapEntry color="#7fe1ff" label="70" opacity="1.0" quantity="70.2525"/>
                            <sld:ColorMapEntry color="#7fdaff" label="71" opacity="1.0" quantity="70.9495"/>
                            <sld:ColorMapEntry color="#7fd3ff" label="72" opacity="1.0" quantity="71.6424"/>
                            <sld:ColorMapEntry color="#7fccff" label="72" opacity="1.0" quantity="72.3394"/>
                            <sld:ColorMapEntry color="#7fc5ff" label="73" opacity="1.0" quantity="73.03229999999999"/>
                            <sld:ColorMapEntry color="#7fbeff" label="74" opacity="1.0" quantity="73.7293"/>
                            <sld:ColorMapEntry color="#7fb7ff" label="74" opacity="1.0" quantity="74.4222"/>
                            <sld:ColorMapEntry color="#7fb0ff" label="75" opacity="1.0" quantity="75.1192"/>
                            <sld:ColorMapEntry color="#7fa9ff" label="76" opacity="1.0" quantity="75.8121"/>
                            <sld:ColorMapEntry color="#7fa2ff" label="77" opacity="1.0" quantity="76.5091"/>
                            <sld:ColorMapEntry color="#7f9fff" label="77" opacity="1.0" quantity="77.202"/>
                            <sld:ColorMapEntry color="#7f98ff" label="78" opacity="1.0" quantity="77.899"/>
                            <sld:ColorMapEntry color="#7f95ff" label="79" opacity="1.0" quantity="78.5919"/>
                            <sld:ColorMapEntry color="#7f8dff" label="79" opacity="1.0" quantity="79.2889"/>
                            <sld:ColorMapEntry color="#7a88fa" label="80" opacity="1.0" quantity="79.98179999999999"/>
                            <sld:ColorMapEntry color="#7583f5" label="81" opacity="1.0" quantity="80.6788"/>
                            <sld:ColorMapEntry color="#707ef0" label="81" opacity="1.0" quantity="81.3717"/>
                            <sld:ColorMapEntry color="#6b79eb" label="82" opacity="1.0" quantity="82.0687"/>
                            <sld:ColorMapEntry color="#6674e6" label="83" opacity="1.0" quantity="82.7616"/>
                            <sld:ColorMapEntry color="#616fe1" label="83" opacity="1.0" quantity="83.4586"/>
                            <sld:ColorMapEntry color="#616fdd" label="84" opacity="1.0" quantity="84.1515"/>
                            <sld:ColorMapEntry color="#616fd9" label="85" opacity="1.0" quantity="84.8485"/>
                            <sld:ColorMapEntry color="#616fd5" label="86" opacity="1.0" quantity="85.5414"/>
                            <sld:ColorMapEntry color="#616fd1" label="86" opacity="1.0" quantity="86.2384"/>
                            <sld:ColorMapEntry color="#616fcd" label="87" opacity="1.0" quantity="86.9313"/>
                            <sld:ColorMapEntry color="#616fc8" label="88" opacity="1.0" quantity="87.6283"/>
                            <sld:ColorMapEntry color="#5e6cc3" label="88" opacity="1.0" quantity="88.3212"/>
                            <sld:ColorMapEntry color="#5c6abe" label="89" opacity="1.0" quantity="89.01820000000001"/>
                            <sld:ColorMapEntry color="#5765b9" label="90" opacity="1.0" quantity="89.7111"/>
                            <sld:ColorMapEntry color="#5260b4" label="90" opacity="1.0" quantity="90.4081"/>
                            <sld:ColorMapEntry color="#4d5baf" label="91" opacity="1.0" quantity="91.101"/>
                            <sld:ColorMapEntry color="#4856aa" label="92" opacity="1.0" quantity="91.798"/>
                            <sld:ColorMapEntry color="#4351a5" label="92" opacity="1.0" quantity="92.4909"/>
                            <sld:ColorMapEntry color="#3e4ca0" label="93" opacity="1.0" quantity="93.1879"/>
                            <sld:ColorMapEntry color="#39479b" label="94" opacity="1.0" quantity="93.8808"/>
                            <sld:ColorMapEntry color="#344296" label="95" opacity="1.0" quantity="94.5778"/>
                            <sld:ColorMapEntry color="#2f3d91" label="95" opacity="1.0" quantity="95.2707"/>
                            <sld:ColorMapEntry color="#2a388c" label="96" opacity="1.0" quantity="95.9677"/>
                            <sld:ColorMapEntry color="#253387" label="97" opacity="1.0" quantity="96.6606"/>
                            <sld:ColorMapEntry color="#202e82" label="97" opacity="1.0" quantity="97.35759999999999"/>
                            <sld:ColorMapEntry color="#1b297d" label="98" opacity="1.0" quantity="98.0505"/>
                            <sld:ColorMapEntry color="#162478" label="99" opacity="1.0" quantity="98.7475"/>
                            <sld:ColorMapEntry color="#111f73" label="99" opacity="1.0" quantity="99.4404"/>
                            <sld:ColorMapEntry color="#0c1a6e" label="100" opacity="1.0" quantity="100.1374"/>
                            <sld:ColorMapEntry color="#070b69" label="101" opacity="1.0" quantity="100.8303"/>
                            <sld:ColorMapEntry color="#020664" label="102" opacity="1.0" quantity="101.5273"/>
                            <sld:ColorMapEntry color="#00015f" label="102" opacity="1.0" quantity="102.2202"/>
                            <sld:ColorMapEntry color="#00005a" label="103" opacity="1.0" quantity="102.91720000000001"/>
                            <sld:ColorMapEntry color="#000055" label="104" opacity="1.0" quantity="103.61009999999999"/>
                            <sld:ColorMapEntry color="#000050" label="104" opacity="1.0" quantity="104.30709999999999"/>
                            <sld:ColorMapEntry color="#000050" label="105" opacity="1.0" quantity="105"/>
                        </sld:ColorMap>
                    </sld:RasterSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>
