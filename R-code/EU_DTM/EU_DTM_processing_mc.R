## Derivation of DTM parameters for EU
## tom.hengl@opengeohub.org

#setwd("/mnt/europa/DTM")
library(rgdal)
#library(fastSave)
dtm.tif = "dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif"
source('DTM_functions.R')
system('/home/envirometrix/software/WBT/whitebox_tools --version')
## WhiteboxTools v1.4.0
system('saga_cmd --version')

## tiling system ----
tiles = readOGR("/mnt/GeoHarmonizer/landmask_30m/tiling_system/tiles_landmask_30km.gpkg")
str(tiles@data)
new.dirs <- paste0("./tt/T", row.names(tiles@data))
x <- lapply(new.dirs, dir.create, recursive=TRUE, showWarnings=FALSE)
pr.dirs <- paste0("T", row.names(tiles@data))
saveRDS(pr.dirs, "pr.dirs30km.rds")

## MrVBF & TWI ----
#system.time( dtm_overlap(id=10286, tiles=tiles, dtm.tif=dtm.tif, out.dir="./tt/", cpus=parallel::detectCores()) )
#system.time( dtm_overlap(id=10287, tiles=tiles, dtm.tif=dtm.tif, out.dir="./tt/", cpus=parallel::detectCores()) )
#system.time( dtm_overlap(id=11980, tiles=tiles, dtm.tif=dtm.tif, out.dir="./tt/", cpus=parallel::detectCores()) )
d = paste(row.names(tiles@data))
dl = split(d, ceiling(seq_along(d)/(length(pr.dirs)/4)))

## run tile by tile but using SAGA GIS / parallelization
x = lapply(dl[[4]], function(i){ try(dtm_overlap(id=i, tiles=tiles, dtm.tif=dtm.tif, out.dir="./tt/", cpus=parallel::detectCores())) } )
#xx = list.files("./tt", pattern = glob2rx("*.*"), full.names = TRUE, recursive = TRUE)
#unlink(xx)
