import sys
sys.path.append('/mnt/europa/tmp/')

import os
import gdal
import traceback
from pathlib import Path
import pandas as pd
import numpy as np
import geopandas as gpd
import rasterio

from uuid import uuid4
from minio import Minio
from minio.error import S3Error

import numexpr as ne

import math
from datetime import datetime 
from pyproj import Proj, transform

from pyeumap.misc import ttprint
from pyeumap.mapper import LandMapper
from pyeumap.mapper import PredictionStrategyType
from pyeumap.tiling import TilingProcessing

def s3_urls(client, host, bucket_name, prefix, ignore=['_flag']):
  urls = []
  
  for images in client.list_objects(bucket_name, prefix=prefix):
    should_append = True
    
    for i in ignore:
      if i in images.object_name:
        should_append = False
        break
    if should_append:
      urls.append(Path(f'http://{host}/{bucket_name}{images.object_name}'))
  
  return urls

def geotemp_imgs(idx, tile, window, base_raster_fn):
  
  def geo_temp(fi, day, a=37.03043, b=-15.43029):
    f =fi
    pi = math.pi 
        
    #math.cos((day - 18) * math.pi / 182.5 + math.pow(2, (1 - math.copysign(1, fi))) * math.pi) 
    sign = 'where(abs(fi) - fi == 0, 1, -1)'
    costeta = f"cos((day - 18) * pi / 182.5 + 2**(1 - {sign}) * pi)"
    
    #math.cos(fi * math.pi / 180)
    cosfi = "cos(fi * pi / 180)"
    A = cosfi

    #(1 - costeta) * abs(math.sin(fi * math.pi / 180) )
    B = f"(1 - {costeta}) * abs(sin(fi * pi / 180) )"

    x = f"a * {A} + b * {B}"
    return ne.evaluate(x)
    
  max_temp_arr = []
  min_temp_arr = []
  
  elev_fn = 'http://192.168.1.57:9000/eumap/dtm/dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif'
  
  with rasterio.open(elev_fn) as ds:
    
    window.round_offsets()
    pixel_size = ds.transform[0]
    lon = np.arange(window.col_off, window.col_off+window.width)
    lat = np.arange(window.row_off, window.row_off+window.height)

    elev_corr = 0.006 * ds.read(1, window=window)

    inProj = Proj(init='epsg:3035')
    outProj = Proj(init='epsg:4326')
    
    lon_grid_3035, lat_grid_3035 = ds.transform * np.meshgrid(lon, lat)
    lon_grid_4326, lat_grid_4326 = transform(inProj, outProj, lon_grid_3035, lat_grid_3035)
    
    for m in range(1,13):
      doy = (datetime.strptime(f'2000-{m}-15', '%Y-%m-%d').timetuple().tm_yday)
      max_temp_arr.append( geo_temp(lat_grid_4326, day=doy, a=37.03043, b=-15.43029) - elev_corr )
      min_temp_arr.append( geo_temp(lat_grid_4326, day=doy, a=24.16453, b=-15.71751) - elev_corr )
  
  return np.stack(max_temp_arr, axis=2), np.stack(min_temp_arr, axis=2)

def spectral_indices_imgs(layernames, layers_data):

  ## spectral indicies functions:
  def ndvi(b3, b4):
      data = ne.evaluate('(b4/255 - b3/255) / (b3/255 + b4/255)')
      data[~np.isfinite(data)] = 0
      return data

  #source: https://www.usgs.gov/core-science-systems/nli/landsat/landsat-enhanced-vegetation-index?qt-science_support_page_related_con=0#qt-science_support_page_related_con
  def evi(b4, b3, b1):
      data = ne.evaluate('2.5 * ((b4/255 - b3/255) / (b4/255 + 6 * b3/255 - 7.5/255 * b1 + 1))')
      data[~np.isfinite(data)] = 0
      return data

  #https://www.usgs.gov/core-science-systems/nli/landsat/landsat-soil-adjusted-vegetation-index
  def savi(b4, b3):
      data = ne.evaluate('((b4/255 - b3/255) / (b4/255 + b3/255 + 0.5)) * (1.5)')
      data[~np.isfinite(data)] = 0
      return data

  #https://www.usgs.gov/core-science-systems/nli/landsat/landsat-modified-soil-adjusted-vegetation-index
  def msavi(b4, b3):
      data = ne.evaluate('(2 * b4/255 + 1 - sqrt(((2 * b4/255 + 1)**2) - (8 * (b4/255 - b3/255)))) / 2')
      data[~np.isfinite(data)] = 0
      return data

  #https://www.usgs.gov/core-science-systems/nli/landsat/normalized-difference-moisture-index
  def ndmi(b4, b5):
      data = ne.evaluate('(b4/255 - b5/255) / (b4/255 + b5/255)')
      data[~np.isfinite(data)] = 0
      return data

  #https://www.usgs.gov/core-science-systems/nli/landsat/landsat-normalized-burn-ratio
  def nbr(b4, b7):
      data = ne.evaluate('(b4/255 - b7/255) / (b4/255 + b7/255)')
      data[~np.isfinite(data)] = 0
      return data

  #https://www.usgs.gov/core-science-systems/nli/landsat/landsat-normalized-burn-ratio-2
  def nbr2(b5, b7):
      data = ne.evaluate('(b5/255 - b7/255) / (b5/255 + b7/255)')
      data[~np.isfinite(data)] = 0
      return data

  def rei(b1, b5, b4):
      data = ne.evaluate('(b4/255 - b1/255) / (b5/255 + (b1/255 * b5/255))')
      data[~np.isfinite(data)] = 0
      return data

  def ndwi(b2, b5):
      data = ne.evaluate('((b2/255 - b5/255) / (b2/255 + b5/255))')
      data[~np.isfinite(data)] = 0
      return data
  
  def str2idx(name):
    for i in range(0,len(layernames)):
      if (name == layernames[i]):
        return i
    return -1000
  
  result_names = []
  result_data = []
  
  for perc in ['p25', 'p50', 'p75']:
    for season in ['winter', 'spring', 'summer', 'fall']:
      b1 = f'lcv_blue_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0'
      b2 = f'lcv_green_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0'
      b3 = f'lcv_red_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0'
      b4 = f'lcv_nir_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0'
      b5 = f'lcv_swir1_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0'
      b6 = f'lcv_swir2_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0'
      b7 = f'lcv_thermal_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0'

      try:
        result_data.append(ndvi(layers_data[:,:,str2idx(b3)], layers_data[:,:,str2idx(b4)]))
        result_names.append(f'lcv_ndvi_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0')
      except Exception as e:
        pass

      #try:
      #  result_data.append(evi(layers_data[:,:,str2idx(b4)], layers_data[:,:,str2idx(b3)], layers_data[:,:,str2idx(b1)]))
      #  result_names.append(f'lcv_evi_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0')
      #except Exception as e:
      #  pass
      
      try:
        result_data.append(savi(layers_data[:,:,str2idx(b4)], layers_data[:,:,str2idx(b3)]))
        result_names.append(f'lcv_savi_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0')
      except Exception as e:
        pass
      
      try:
        result_data.append(msavi(layers_data[:,:,str2idx(b4)], layers_data[:,:,str2idx(b3)]))
        result_names.append(f'lcv_msavi_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0')
      except Exception as e:
        pass
      
      try:
        result_data.append(ndmi(layers_data[:,:,str2idx(b4)], layers_data[:,:,str2idx(b5)]))
        result_names.append(f'lcv_ndmi_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0')
      except Exception as e:
        pass
      
      try:
        result_data.append(nbr(layers_data[:,:,str2idx(b4)], layers_data[:,:,str2idx(b7)]))
        result_names.append(f'lcv_nbr_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0')
      except Exception as e:
        pass
      
      try:
        result_data.append(nbr2(layers_data[:,:,str2idx(b5)], layers_data[:,:,str2idx(b7)]))
        result_names.append(f'lcv_nbr2_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0')
      except Exception as e:
        pass
      
      #try:
        #result_data.append(rei(layers_data[:,:,str2idx(b1)], layers_data[:,:,str2idx(b5)], layers_data[:,:,str2idx(b4)]))
        #result_names.append(f'lcv_rei_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0')
      #except Exception as e:
      #  pass
      
      try:
        result_data.append(ndwi(layers_data[:,:,str2idx(b2)], layers_data[:,:,str2idx(b5)]))
        result_names.append(f'lcv_ndwi_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.0')
      except Exception as e:
        pass

  
  return result_names, np.stack(result_data, axis=2)

def process_tile(idx, tile, window, base_raster_fn, landmapper, 
  s3_host, s3_access_key, s3_access_secret, s3_bucket_name):
    
  def inmem_calc(layernames, layers_data, spatial_win):
    
    layers_data = np.append(layers_data, max_temp_img, axis=2)
    layernames += [f'clm_lst_max.geom.temp_m_30m_s0..0cm_m{m}' for m in range(1,13)]
    
    layers_data = np.append(layers_data, min_temp, axis=2)
    layernames += [f'clm_lst_min.geom.temp_m_30m_s0..0cm_m{m}' for m in range(1,13)]
    
    indices_layers, indices_data = spectral_indices_imgs(layernames, layers_data)
    
    layers_data = np.append(layers_data, indices_data, axis=2)
    layernames += indices_layers

    return layernames, layers_data
  
  # Necessary to avoid reading tiles with diferent sizes
  window = window.round_offsets()

  tmp_dir = f'result/{idx}'
  
  landsat_pref = f'/lcv'
  night_lights_pref = f'/lcv/lcv_night.light_suomi.npp.viirs_avg.rade9h_30m_0..0cm'
  dtm_pref = f'/dtm/dtm'
  hyd_pref = f'/hyd/hyd'
  out_pref = f'tmp/eumap/lc_r3/{idx}'
  
  Path(tmp_dir).mkdir(parents=True, exist_ok=True)
  client = Minio(s3_host, s3_access_key, s3_access_secret, secure=False)
  
  urls = s3_urls(client, s3_host, s3_bucket_name, f'/{out_pref}/')
  if False and len(urls) == 20:
    ttprint(f'Ignoring tile {idx}. Already processed.')
    return

  try:
    ttprint('Generating geometric temperature layers on the fly')
    max_temp_img, min_temp = geotemp_imgs(idx, tile, window, base_raster_fn)
    
    dtm_urls = s3_urls(client, s3_host, s3_bucket_name, dtm_pref, ['_50m_', '_100m_','_250m_'])
    hyd_urls = s3_urls(client, s3_host, s3_bucket_name, hyd_pref)
    
    fn_layers_list = []
    fn_result_list = []
    dict_layers_newnames_list = []
    
    for year in range(2000,2020):
      
      year_night_lights_pref = f'{night_lights_pref}_{year}'

      year_landsat_urls = []
      dict_layers_newnames = {}
      for b in ['blue', 'green', 'red', 'nir', 'swir1', 'swir2', 'thermal']:
        for p in ['p25', 'p50', 'p75']:
          landsat_winter_pref = f'lcv_{b}_landsat.glad.ard_{p}_30m_0..0cm_{year-1}1202'
          landsat_spring_pref = f'lcv_{b}_landsat.glad.ard_{p}_30m_0..0cm_{year}0321'
          landsat_summer_pref = f'lcv_{b}_landsat.glad.ard_{p}_30m_0..0cm_{year}0625'
          landsat_fall_pref = f'lcv_{b}_landsat.glad.ard_{p}_30m_0..0cm_{year}0913'

          landsat_winter_urls = s3_urls(client, s3_host, s3_bucket_name, f'{landsat_pref}/{landsat_winter_pref}')
          landsat_spring_urls = s3_urls(client, s3_host, s3_bucket_name, f'{landsat_pref}/{landsat_spring_pref}')
          landsat_summer_urls = s3_urls(client, s3_host, s3_bucket_name, f'{landsat_pref}/{landsat_summer_pref}')
          landsat_fall_urls   = s3_urls(client, s3_host, s3_bucket_name, f'{landsat_pref}/{landsat_fall_pref}')

          year_landsat_urls += landsat_winter_urls
          year_landsat_urls += landsat_spring_urls
          year_landsat_urls += landsat_summer_urls
          year_landsat_urls += landsat_fall_urls
          
          dict_layers_newnames[f'lcv_{b}_landsat.glad.ard_{p}_30m_0..0cm_winter_eumap_epsg3035_v1.0'] = landsat_winter_pref + f'..{year}0320_eumap_epsg3035_v1.0'
          dict_layers_newnames[f'lcv_{b}_landsat.glad.ard_{p}_30m_0..0cm_spring_eumap_epsg3035_v1.0'] = landsat_spring_pref + f'..{year}0624_eumap_epsg3035_v1.0'
          dict_layers_newnames[f'lcv_{b}_landsat.glad.ard_{p}_30m_0..0cm_summer_eumap_epsg3035_v1.0'] = landsat_summer_pref + f'..{year}0912_eumap_epsg3035_v1.0'
          dict_layers_newnames[f'lcv_{b}_landsat.glad.ard_{p}_30m_0..0cm_fall_eumap_epsg3035_v1.0'] = landsat_fall_pref + f'..{year}1201_eumap_epsg3035_v1.0'
      
      dict_layers_newnames['lcv_night.light_suomi.npp.viirs_avg.rade9h_30m_0..0cm_eumap_epsg3035_v0.1'] = 'lcv_night.light_suomi.npp.viirs_avg.rade9h_30m_0..0cm_'+str(year)+'[0-9]{4}..'+str(year)+'[0-9]{4}_eumap_epsg3035_v0.1'
      
      year_night_lights_urls = s3_urls(client, s3_host, s3_bucket_name, year_night_lights_pref)
      
      year_urls = dtm_urls + hyd_urls + year_night_lights_urls + year_landsat_urls

      fn_layers_list.append(year_urls)
      fn_result_list.append(os.path.join(tmp_dir,f'lc_{year}.tif'))
      dict_layers_newnames_list.append(dict_layers_newnames)
    
    # One year test
    # output_fn_files = landmapper.predict(fn_layers=fn_layers_list[0], fn_output=fn_result_list[0], spatial_win=window, \
    #                    inmem_calc_func=inmem_calc, dict_layers_newnames=dict_layers_newnames_list[0], \
    #                    allow_aditional_layers=True, hard_class=True)

    output_fn_files = landmapper.predict_multi(fn_layers_list=fn_layers_list, fn_output_list=fn_result_list, spatial_win=window, \
                        inmem_calc_func=inmem_calc, dict_layers_newnames_list=dict_layers_newnames_list, \
                        allow_aditional_layers=True, hard_class=True, prediction_strategy_type = PredictionStrategyType.Lazy)

    for output_fn_file in output_fn_files:
     object_name = f'{Path(output_fn_file).name}'
     object_bucket = f'{s3_bucket_name}'
     ttprint(f'Copying {output_fn_file} to http://{s3_host}/{object_bucket}/{out_pref}/{object_name}')
     client.fput_object(object_bucket, f'{out_pref}/{object_name}', output_fn_file)
     os.remove(output_fn_file)

  except:
    tb = traceback.format_exc()
    ttprint(f'ERROR: Tile {idx} failed.')
    ttprint(tb)

# S3/Min.io credentials
s3_host = "192.168.1.57:9000" # Gaia server
s3_access_key = ""
s3_access_secret = ""
s3_bucket_name = 'eumap'

if s3_access_key == "" or s3_access_secret == "":
  print("You need setup the S3 credentials in s3_access_key and s3_access_secret variables")
  exit()

# TF CPU optimization
# https://www.tensorflow.org/api_docs/python/tf/config/threading/set_inter_op_parallelism_threads
import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(1) 
tf.config.threading.set_intra_op_parallelism_threads(1)
tf.config.set_soft_device_placement(True)

# Downloand the LandMapper instance in https://doi.org/10.5281/zenodo.4725429
fn_landmapper = 'lcv_landcover.hcl_lucas.corine.eml_p_landmapper_light.lz4'
ttprint(f'Loading {fn_landmapper}')
landmapper = LandMapper.load_instance(fn_landmapper)

base_raster_fn = f'http://{s3_host}/{s3_bucket_name}/lcv/lcv_red_landsat.glad.ard_p50_30m_0..0cm_19991202..20000320_eumap_epsg3035_v1.0.tif'
processing = TilingProcessing(base_raster_fn=base_raster_fn, pixel_precision=0)

func_args = [base_raster_fn, landmapper, s3_host, s3_access_key, s3_access_secret, s3_bucket_name]

########################################################################
### Test one tile
########################################################################
processing.process_one(625, process_tile, func_args=func_args)

########################################################################
### Production code spliting tiles by server
########################################################################
# Atlas
#processing.process_multiple(range(0,1400), process_tile, func_args=func_args, max_workers=1)
# Silva
#processing.process_multiple(range(1400,2800), process_tile, func_args=func_args, max_workers=1)
# Apollo
#processing.process_multiple(range(2800,4200), process_tile, func_args=func_args, max_workers=1)
# Landmark
#processing.process_multiple(range(4200,5600), process_tile, func_args=func_args, max_workers=1)
# Europa
#processing.process_multiple(range(5600,7042), process_tile, func_args=func_args, max_workers=1)